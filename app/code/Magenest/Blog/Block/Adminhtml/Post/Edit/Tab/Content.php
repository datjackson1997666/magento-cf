<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Blog\Block\Adminhtml\Post\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;

/**
 * Admin blog post edit form content tab
 */
class Content extends Generic implements TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param WysiwygConfig $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        WysiwygConfig $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var $model \Magenest\Blog\Model\Post */
        $model = $this->_coreRegistry->registry('blog_posts');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('post_');

        $fieldset = $form->addFieldset(
            'content_fieldset',
            ['legend' => __('Content'), 'class' => 'fieldset-wide']
        );

        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
        $fieldset->addField(
            'image_content',
            'image',
            [
                'name' => 'image_content',
                'label' => __('Image'),
                'required' => false,
            ]
        );
        $fieldset->addField(
            'short_content',
            'editor',
            [
                'name' => 'post[short_content]',
                'label' => __('Short Content'),
                'title' => __('Short Content'),
                'style' => 'height:5em;',
                'wysiwyg' => true,
                'config' => $wysiwygConfig
            ]
        );
        $fieldset->addField(
            'tags',
            'text',
            [
                'name' => 'post[tags]',
                'label' => __('Tags'),
                'title' => __('Tags'),
                'note' => __('A tag is a keyword or label that posts'),
            ]
        );
        $fieldset->addField(
            'content',
            'editor',
            [
                'name' => 'post[content]',
                'label' => __('Content'),
                'title' => __('Content'),
                'style' => 'height:36em;',
                'wysiwyg' => true,
                'config' => $wysiwygConfig
            ]
        );

        $this->_eventManager->dispatch('magenest_blog_post_edit_tab_content_prepare_form', ['form' => $form]);
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Content');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Content');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
