<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Blog\Block\Adminhtml\Post\Edit\Tab;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Store\Model\System\Store as SystemStore;
use Magenest\Blog\Model\Status;
use Magenest\Blog\Model\ResourceModel\Category\Collection;

/**
 * Class Main
 * @package Magenest\Blog\Block\Adminhtml\Post\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
    /**
     * @var \Magenest\Blog\Model\Status
     */
    protected $_status;

    /**
     * @var DateTime
     */
    protected $_dateTime;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param SystemStore $systemStore
     * @param Status $status
     * @param DateTime $dateTime
     * @param Collection $categoryCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        SystemStore $systemStore,
        Status $status,
        DateTime $dateTime,
        Collection $categoryCollection,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->_dateTime = $dateTime;
        $this->_categoryCollection = $categoryCollection;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Magenest\Blog\Model\Post */
        $model = $this->_coreRegistry->registry('blog_posts');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Post Information')]);

        if ($model->getId()) {
            $fieldset->addField(
                'post_id',
                'hidden',
                ['name' => 'post[id]']
            );
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'post[title]',
                'label' => __('Post Title'),
                'title' => __('Post Title'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'identifier',
            'text',
            [
                'name' => 'post[identifier]',
                'label' => __('URL Key'),
                'title' => __('URL Key'),
                'required' => true,
                'class' => 'validate-identifier',
                'note' => __('Relative to Web Site Base URL')
            ]
        );

        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'multiselect',
                [
                    'name' => 'post[stores][]',
                    'label' => __('Store View'),
                    'title' => __('Store View'),
                    'required' => true,
                    'values' => $this->_systemStore->getStoreValuesForForm(false, true)
                ]
            );
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                ['name' => 'post[stores][]', 'values' => $this->_storeManager->getStore(true)->getId()]
            );
            $model->setStoreId($this->_storeManager->getStore(true)->getId());
        }

        $categories[] = ['label' => __('No Category'), 'value' => ''];
        $collection = $this->_categoryCollection->addActiveFilter();

        foreach ($collection as $item) {
            $categories[] = [
                'label' => $item->getTitle(),
                'value' => $item->getId(),
            ];
        }

        $fieldset->addField(
            'categories',
            'multiselect',
            [
                'name' => 'post[categories][]',
                'label' => __('Categories'),
                'title' => __('Categories'),
                'values' => $categories,
                'style' => 'width:50%',
            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Post Status'),
                'name' => 'post[status]',
                'required' => true,
                'options' => $this->_status->getOptionArray()
            ]
        );

        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::SHORT
        );
        $timeFormat = $this->_localeDate->getTimeFormat(
            \IntlDateFormatter::SHORT
        );

        $fieldset->addField(
            'publish_time',
            'date',
            [
                'name' => 'post[publish_time]',
                'label' => __('Publish At'),
                'date_format' => $dateFormat,
                'time_format' => $timeFormat,
                'class' => 'validate-date validate-date-range date-range-custom_theme-from',
                'value' => $this->_dateTime->gmtDate(),
            ]
        );
        $fieldset->addField(
            'user',
            'text',
            [
                'name' => 'post[user]',
                'label' => __('Username'),
                'title' => __('Username'),
            ]
        );
        if ($model->getId()) {
            $form->setValues($model->getData());
        }
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
