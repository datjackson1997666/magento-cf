<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Block;

use Magento\Store\Model\ScopeInterface;
use Magenest\Blog\Block\Post\ListPost;

/**
 * Class Index
 *
 * @package Magenest\Blog\Block
 */
class Index extends ListPost
{
    /**
     * Prepare Collection
     *
     * @return $this|\Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostCollection()
    {
        parent::getPostCollection();
        $tag = $this->_request->getParam('tag');

        if ($tag) {
            $tag = str_replace('+', '', $tag);
            $collection = $this->_postCollection->addFieldToFilter('tags', ['like' => '%'.$tag.'%']);
            return $collection;
        }

        return $this->_postCollection;
    }
    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->_addBreadcrumbs();
        $this->pageConfig->setPageLayout($this->_getConfigValue('page_layout'));
        $this->pageConfig->getTitle()->set($this->_getConfigValue('title'));
        $this->pageConfig->setKeywords($this->_getConfigValue('meta_keywords'));
        $this->pageConfig->setDescription($this->_getConfigValue('meta_description'));
        $this->pageConfig->setRobots($this->_getConfigValue('meta_robots'));

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs()
    {
        if ($this->_scopeConfig->getValue('web/default/show_cms_breadcrumbs', ScopeInterface::SCOPE_STORE)
            && ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs'))
        ) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb(
                'blog',
                [
                    'label' => __('Blog'),
                    'title' => __(sprintf('Go to Blog Home Page'))
                ]
            );
        }
    }

    /**
     * @param $param
     * @return mixed
     */
    protected function _getConfigValue($param)
    {
        return $this->_scopeConfig->getValue(
            'blog/index_page/'.$param,
            ScopeInterface::SCOPE_STORE
        );
    }
}
