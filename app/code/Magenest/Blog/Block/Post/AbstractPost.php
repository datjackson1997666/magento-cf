<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Post;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Blog\Model\PostFactory;
use Magento\Framework\UrlInterface;

/**
 * Abstract post
 */
abstract class AbstractPost extends Template
{

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magenest\Blog\Model\Post
     */
    protected $_post;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Page factory
     *
     * @var \Magenest\Blog\Model\PostFactory
     */
    protected $_postFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var string
     */
    protected $_defaultPostInfoBlock = 'Magenest\Blog\Block\Post\Info';

    /**
     * @var string
     */
    protected $_defaultPostCommentsBlock = 'Magenest\Blog\Block\Post\View\Comments';

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FilterProvider $filterProvider
     * @param PostFactory $postFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FilterProvider $filterProvider,
        PostFactory $postFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
        $this->_filterProvider = $filterProvider;
        $this->_postFactory = $postFactory;
    }

    /**
     * Retrieve post instance
     *
     * @return \Magenest\Blog\Model\Post
     */
    public function getPost()
    {
        if (!$this->hasData('post')) {
            $this->setData(
                'post',
                $this->_coreRegistry->registry('current_blog_post')
            );
        }

        return $this->getData('post');
    }

    /**
     * @param $post
     * @return $this
     */
    public function setPost($post)
    {
        $this->setData('post', $post);
        return $this;
    }
    /**
     * Retrieve post short content
     *
     * @return string
     */
    public function getShorContent()
    {
        $short_content = $this->getPost()->getShortContent();
        return $this->_filterProvider->getPageFilter()->filter($short_content);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getContent()
    {
        $content = $this->getPost()->getContent();
        return $this->_filterProvider->getPageFilter()->filter($content);
    }

    /**
     * @return array
     */
    public function getTags()
    {
        $tags = $this->getPost()->getTag();
        $tags = array_map('trim', explode(',', $tags));
        return $tags;
    }

    /**
     * @param $tag
     * @return string
     */
    public function getTagsUrl($tag)
    {
        $tag = str_replace(' ', '+', $tag);
        $url = $this->getBaseUrl().'blog/tag/'.$tag;

        return $url;
    }
    /**
     * Retrieve post info html
     *
     * @return string
     */
    public function getInfoHtml()
    {
        return $this->getInfoBlock()->toHtml();
    }

    /**
     * Retrieve post info block
     *
     * @return \Magenest\Blog\Block\Post\Info
     */
    public function getInfoBlock()
    {
        $info = 'info_block';
        if (!$this->hasData($info)) {
            $blockName = $this->getPostInfoBlockName();
            if ($blockName) {
                $block = $this->getLayout()->getBlock($blockName);
            }

            if (empty($block)) {
                $block = $this->getLayout()->createBlock($this->_defaultPostInfoBlock, uniqid(microtime()));
            }

            $this->setData($info, $block);
        }

        return $this->getData($info)->setPost($this->getPost());
    }

    /**
     * Get Media Base URL
     *
     * @return string
     */
    protected function getBaseMedia()
    {
        return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     *
     * @param $path
     * @return string
     */
    public function getImageUrl($path)
    {
        $baseMediaUrl = $this->getBaseMedia();

        return $baseMediaUrl.$path;
    }
}
