<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV>-thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Post;

use Magento\Framework\View\Element\Template;

/**
 * Class Info
 *
 * @package Magenest\Blog\Block\Post
 */
class Info extends Template
{
    /**
     * Block template file
     *
     * @var string
     */
    protected $_template = 'post/info.phtml';

    /**
     * Retrieve formated posted date
     *
     * @var string
     * @return string
     */
    public function getPostedOn($format = 'Y-m-d H:i:s')
    {
        return date($format, strtotime($this->getPost()->getData('publish_time')));
    }
}
