<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV>-thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Post;

use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magenest\Blog\Block\Post\PostList\AbstractList;

/**
 * Class ListPost
 * @package Magenest\Blog\Block\Post
 */
class ListPost extends AbstractList
{
    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'Magenest\Blog\Block\Post\PostList\Toolbar';

    /**
     * Retrieve post html
     * @param  \Magenest\Blog\Model\Post $post
     * @return string
     */
    public function getPostHtml($post)
    {
        return $this->getLayout()->createBlock('Magenest\Blog\Block\Post\PostList\Item')->setPost($post)->toHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return \Magenest\Blog\Block\Post\PostList\Toolbar
     */
    public function getToolbarBlock()
    {
        $blockName = $this->getToolbarBlockName();
        if ($blockName) {
            $block = $this->getLayout()->getBlock($blockName);
            if ($block) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, uniqid(microtime()));
        return $block;
    }

    /**
     * Retrieve Toolbar Html
     *
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    /**
     * Before block to html
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $toolbar = $this->getToolbarBlock();
        $collection = $this->getPostCollection();
        $toolbar->setCollection($collection);
        $this->setChild('toolbar', $toolbar);
        $this->getPostCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * @param AbstractCollection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_postCollection = $collection;
        return $this;
    }
}
