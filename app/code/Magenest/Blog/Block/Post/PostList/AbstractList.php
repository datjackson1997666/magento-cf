<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author <ThaoPV>-thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Post\PostList;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Blog\Model\ResourceModel\Post\CollectionFactory;
use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Abstract blog post list block
 */
abstract class AbstractList extends Template
{
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $_post;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magenest\Blog\Model\ResourceModel\Post\CollectionFactory
     */
    protected $_postCollectionFactory;

    /**
     * @var \Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    protected $_postCollection;

    /**
     * @var DateTime
     */
    protected $_coreDate;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FilterProvider $filterProvider
     * @param CollectionFactory $postCollectionFactory
     * @param DateTime $coreDate
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FilterProvider $filterProvider,
        CollectionFactory $postCollectionFactory,
        DateTime $coreDate,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
        $this->_filterProvider = $filterProvider;
        $this->_postCollectionFactory = $postCollectionFactory;
        $this->_coreDate = $coreDate;
    }

    /**
     * Prepare posts collection
     *
     * @return void
     */
    public function _preparePostCollection()
    {
        $publishTime = [
            'or' => [
                0 => ['date' => true, 'to' => $this->_coreDate->gmtDate()],
                1 => ['is' => new \Zend_Db_Expr('null')],
            ]
        ];

        $this->_postCollection = $this->_postCollectionFactory->create()
            ->addActiveFilter()
            ->addStoreFilter()
            ->addFieldToFilter('publish_time', $publishTime)
            ->setOrder('publish_time', 'DESC')
        ;
    }

    /**
     * Prepare posts collection
     *
     * @return \Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostCollection()
    {
        if (is_null($this->_postCollection)) {
            $this->_preparePostCollection();
        }
        return $this->_postCollection;
    }
}
