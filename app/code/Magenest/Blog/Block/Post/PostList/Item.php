<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author <ThaoPV>-thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Post\PostList;

use Magenest\Blog\Block\Post\AbstractPost;

/**
 * Post list item
 */
class Item extends AbstractPost
{
    /**
     * Default Template
     *
     * @var string
     */
    protected $_template = "Magenest_Blog::post/list/item.phtml";
}
