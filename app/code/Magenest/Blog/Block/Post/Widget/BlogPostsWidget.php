<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_QuickBooksOnline extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\Blog\Block\Post\Widget;

use Magenest\Blog\Block\Post\ListPost;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class BlogPostsWidget
 * @package Magenest\Blog\Block\Post\Widget
 */
class BlogPostsWidget extends ListPost implements BlockInterface
{
    /**
     * Default value whether show pager or not
     */
    const DEFAULT_SHOW_PAGER = false;

    /**
     * Default value for posts per page
     */
    const DEFAULT_BLOG_POSTS_PER_PAGE = 3;

    /**
     * Name of request parameter for page number value
     */
    const PAGE_VAR_NAME = 'np';

    /**
     * @return \Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    public function getCollection()
    {
        $collections = $this->_postCollection->setPageSize($this->getPageSize())->setCurPage(1);

        return $collections;
    }

    /**
     * Get Title Widget
     *
     * @return mixed
     */
    public function getTitle()
    {
        if (!$this->hasData('title')) {
            $this->setData('title', __('From The Blog'));
        }

        return $this->getData('title');
    }

    /**
     * Page Size
     *
     * @return mixed
     */
    public function getPageSize()
    {
        if (!$this->hasData('blog_displays')) {
            $this->setData('blog_displays', self::DEFAULT_BLOG_POSTS_PER_PAGE);
        }

        return $this->getData('blog_displays');
    }

    /**
     * Add data to the widget.
     * Retains previous data in the widget.
     *
     * @param array $arr
     * @return $this
     */
    public function addData(array $arr)
    {
        return parent::addData($arr);
    }

    /**
     * Overwrite data in the widget.
     *
     * Param $key can be string or array.
     * If $key is string, the attribute value will be overwritten by $value.
     * If $key is an array, it will overwrite all the data in the widget.
     *
     * @param string|array $key
     * @param mixed $value
     * @return \Magento\Framework\DataObject
     */
    public function setData($key, $value = null)
    {
        return parent::setData($key, $value);
    }
}
