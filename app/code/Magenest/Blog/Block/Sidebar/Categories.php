<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Sidebar;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Blog\Model\ResourceModel\Category\Collection;
use Magento\Cms\Model\Block;

/**
 * Class Categories
 * @package Magenest\Blog\Block\Sidebar
 */
class Categories extends Template
{
    /**
     * @var \Magenest\Blog\Model\ResourceModel\Category\Collection
     */
    protected $_categoryCollection;

    /**
     * @param Context $context
     * @param Collection $categoryCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Collection $categoryCollection,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_categoryCollection = $categoryCollection;
    }

    /**
     * Get grouped categories
     * @return \Magenest\Blog\Model\ResourceModel\Category\Collection
     */
    protected function getGroupedChilds()
    {
        $key = 'grouped_childs';
        if (!$this->hasData($key)) {
            $array = $this->_categoryCollection
                ->addActiveFilter()
                ->addStoreFilter($this->_storeManager->getStore()->getId());

            $this->setData($key, $array);
        }

        return $this->getData($key);
    }

    /**
     * @return string
     */
    public function getCategoryToHtml()
    {
        $collections = $this->getGroupedChilds();
        $html = '';
        if ($collections->count()) {
            /** @var \Magenest\Blog\Model\Category $collection */
            foreach ($collections as $collection) {
                $html .= '<li class="category-item">';
                $html .= '<a href="'.$collection->getCategoryUrl().'">';
                $html .= $this->escapeHtml($collection->getTitle());
                $html .= '</a>';
                $html .= '</li>';
            }
        }

        return $html;
    }

    /**
     * Check Sidebar
     */
    public function isActive()
    {
        return $this->_scopeConfig->getValue('blog/sidebar/categories/enabled');
    }
}
