<?php
/**
 * Copyright Â© 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Sidebar;

use Magenest\Blog\Block\Post\PostList\AbstractList;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Recent
 * @package Magenest\Blog\Block\Sidebar
 */
class Recent extends AbstractList
{
    /**
     * @var string
     */
    protected $_widgetKey = 'recent_posts';

    /**
     * Collection
     *
     * @return \Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPostCollection()
    {
        parent::getPostCollection();
        $collection = $this->_postCollection->setPageSize(
            (int) $this->_scopeConfig->getValue(
            'blog/sidebar/'.$this->_widgetKey.'/posts_per_page',
            ScopeInterface::SCOPE_STORE
        ));

        return $collection;
    }

    /**
     * Check Sidebar
     */
    public function isActive()
    {
        return $this->_scopeConfig->getValue('blog/sidebar/recent_posts/enabled');
    }
}
