<?php
/**
 * Copyright Â© 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Block\Sidebar;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Magenest\Blog\Model\ResourceModel\Tags\Collection;


/**
 * Class Recent
 * @package Magenest\Blog\Block\Sidebar
 */
class Tags extends Template
{
    /**
     * @var Collection
     */
    protected $_collection;

    /**
     * @param Template\Context $context
     * @param Collection $collection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Collection $collection,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_collection = $collection;
    }
    /**
     * @param $tag
     * @return string
     */
    public function getTagsUrl($tag)
    {
        $tag = str_replace(' ', '+', $tag);
        $url = $this->getBaseUrl().'blog/tag/'.$tag;

        return $url;
    }

    /**
     * Collection
     *
     * @return \Magenest\Blog\Model\ResourceModel\Post\Collection
     */
    public function getTagsCollection()
    {
        $collection = $this->_collection->setPageSize(
            (int) $this->_scopeConfig->getValue(
                'blog/sidebar/tags/numbers_tags',
                ScopeInterface::SCOPE_STORE
            ));

        return $collection;
    }

    /**
     * Check Sidebar
     */
    public function isActive()
    {
        return $this->_scopeConfig->getValue('blog/sidebar/tags/enabled');
    }
}
