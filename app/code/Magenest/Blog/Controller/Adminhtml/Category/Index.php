<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Controller\Adminhtml\Category;


use Magenest\Blog\Controller\Adminhtml\Category as AbstractCategory;

class Index extends AbstractCategory
{
    /**
     * execute the action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend((__('Categories')));
        return $resultPage;
    }
}
