<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author <ThaoPV>-thaopw@gmail.com
 */
namespace Magenest\Blog\Controller\Adminhtml\Post;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Blog\Model\PostFactory;
use Magenest\Blog\Model\ResourceModel\Post\CollectionFactory as CollectionFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Blog\Controller\Adminhtml\Post as AbstractPost;
use Magento\Backend\Model\Auth\Session as BackendSession;
use Magento\Framework\Exception\LocalizedException;
use Magenest\Blog\Helper\Upload as UploadHelper;

/**
 * Class Save
 * @package Magenest\Blog\Controller\Adminhtml\Post
 */
class Save extends AbstractPost
{
    /**
     * @var UploadHelper
     */
    protected $_uploadHelper;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_backendAuthSession;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param PostFactory $postFactory
     * @param CollectionFactory $collectionFactory
     * @param ForwardFactory $resultForwardFactory
     * @param BackendSession $backendAuthSession
     * @param Filter $filter
     * @param UploadHelper $uploadHelper
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        PostFactory  $postFactory,
        CollectionFactory $collectionFactory,
        ForwardFactory $resultForwardFactory,
        BackendSession $backendAuthSession,
        Filter $filter,
        UploadHelper $uploadHelper
    ) {
        $this->_backendAuthSession = $backendAuthSession;
        $this->_uploadHelper = $uploadHelper;
        parent::__construct($context, $coreRegistry, $resultPageFactory, $postFactory, $collectionFactory, $resultForwardFactory, $filter);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_postFactory->create();
            $uploadFiles = [];
            if (!empty($data['image_content'])) {
                $uploadFiles = $data['image_content'];
            }

            $data = $data['post'];
            if (isset($data['id'])) {
                $model->load($data['id']);
                if ($data['id'] != $model->getId()) {
                    throw new LocalizedException(__('Unable load Post.'));
                }
            }
            if (empty($data['user'])) {
                $data['user'] = $this->_backendAuthSession->getUser()->getName();
            }

            $tags = $data['tags'];
            if ($tags) {
                $tags = array_map('trim', explode(',', $tags));
                foreach ($tags as $tag) {
                    $tagsModel = $this->_objectManager->create('Magenest\Blog\Model\Tags');
                    $tagsModel->loadName($tag);
                    $tagsModel->addData(['name' => $tag]);
                    $tagsModel->save();
                }
            }
            /** Save image */
            $data['image_content'] = $this->_uploadHelper->uploadImage($uploadFiles);
            $model->addData($data);
            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Post has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving the Post.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($data);
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
