<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category  Magenest
 * @package   Magenest_Blog
 * @author ThaoPV-<thaopw@gmail.com>
 */
namespace Magenest\Blog\Controller\Index;

use Magento\Framework\App\Action\Action;

/**
 * Class Index
 * @package Magenest\Blog\Controller\Index
 */
class Index extends Action
{
    /**
     * execute
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Blog'));
        $this->_view->renderLayout();
    }
}
