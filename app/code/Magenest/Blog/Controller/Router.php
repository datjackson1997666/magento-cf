<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Controller;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magenest\Blog\Model\PostFactory;
use Magenest\Blog\Model\CategoryFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ResponseInterface;

/**
 * Blog Controller Router
 *
 * @method RequestInterface string getPathInfo()
 */
class Router implements RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    /**
     * Event manager
     *
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Page factory
     *
     * @var \Magenest\Blog\Model\PostFactory
     */
    protected $_postFactory;

    /**
     * Category factory
     *
     * @var \Magenest\Blog\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * Config primary
     *
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * Url
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * Response
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @param ActionFactory $actionFactory
     * @param ManagerInterface $eventManager
     * @param UrlInterface $url
     * @param PostFactory $postFactory
     * @param CategoryFactory $categoryFactory
     * @param StoreManagerInterface $storeManager
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ManagerInterface $eventManager,
        UrlInterface $url,
        PostFactory $postFactory,
        CategoryFactory $categoryFactory,
        StoreManagerInterface $storeManager,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_eventManager = $eventManager;
        $this->_url = $url;
        $this->_postFactory = $postFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_storeManager = $storeManager;
        $this->_response = $response;
    }

    /**
     * Validate and Match Blog Pages and modify request
     *
     * @param \Magento\Framework\App\RequestInterface $request
     * @return bool
     */
    public function match(RequestInterface $request)
    {
        $_identifier = trim($request->getPathInfo(), '/');

        if (strpos($_identifier, 'blog') !== 0) {
            return;
        }

        $identifier = str_replace('blog/', '', $_identifier);

        $condition = new \Magento\Framework\DataObject(['identifier' => $identifier, 'continue' => true]);

        if ($condition->getRedirectUrl()) {
            $this->_response->setRedirect($condition->getRedirectUrl());
            $request->setDispatched(true);
            return $this->actionFactory->create(
                'Magento\Framework\App\Action\Redirect',
                ['request' => $request]
            );
        }

        if (!$condition->getContinue()) {
            return null;
        }

        $identifier = $condition->getIdentifier();

        $success = false;
        $info = explode('/', $identifier);

        if (!$identifier) {
            $request->setModuleName('blog')
                ->setControllerName('index')
                ->setActionName('index');
            $success = true;
        } elseif (count($info) > 1) {
            switch ($info[0]) {
                case 'post':
                    $post = $this->_postFactory->create();
                    $postId = $post->loadByIdentifier($info[1])->getId();
                    if (!$postId) {
                        return null;
                    }

                    $request->setModuleName('blog')
                        ->setControllerName('post')
                        ->setActionName('view')
                        ->setParam('id', $postId);
                    $success = true;
                    break;
                case 'category':
                    $category = $this->_categoryFactory->create();
                    $categoryId = $category->loadByIdentifier($info[1])->getId();
                    if (!$categoryId) {
                        return null;
                    }

                    $request->setModuleName('blog')
                        ->setControllerName('category')
                        ->setActionName('view')
                        ->setParam('id', $categoryId);
                    $success = true;
                    break;
                case 'tag':
                    if(isset($info[1])) {
                        $request->setModuleName('blog')->setControllerName('index')
                                ->setActionName('index')
                                ->setParam('tag',  $info[1]);
                        $success = true;
                    }
                        break;

                default:
                    break;
            }

        }

        if (!$success) {
            return null;
        }

        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $_identifier);

        return $this->actionFactory->create(
            'Magento\Framework\App\Action\Forward',
            ['request' => $request]
        );
    }
}
