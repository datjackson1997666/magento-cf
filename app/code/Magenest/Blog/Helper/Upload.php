<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Helper;

use Magento\Framework\Filesystem;
use Magento\Backend\Model\Auth\Session;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Upload
 * @package Magenest\Blog\Helper
 */
class Upload
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var UploaderFactory
     */
    protected $_uploaderFactory;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @param RequestInterface $request
     * @param UploaderFactory $uploaderFactory
     * @param Filesystem $filesystem
     * @param LoggerInterface $logger
     */
    public function __construct(
        RequestInterface $request,
        UploaderFactory $uploaderFactory,
        Filesystem $filesystem,
        LoggerInterface $logger
    ) {
        $this->_request = $request;
        $this->_uploaderFactory = $uploaderFactory;
        $this->_filesystem = $filesystem;
        $this->_logger = $logger;
    }

    /**
     * Upload image to server
     *
     * @param array $data
     * @return string|void
     */
    public function uploadImage($data)
    {
        $file = $this->_request->getFiles('image_content');
        if ($file || !empty($data)) {
            /** Deleted file */
            if (!empty($data['delete']) && ! empty($data['value'])) {
                $path = $this->_filesystem->getDirectoryRead(
                    DirectoryList::MEDIA
                );
                if ($path->isFile($data['value'])) {
                    $this->_filesystem->getDirectoryWrite(
                        DirectoryList::MEDIA
                    )->delete($data['value']);
                }
                if (empty($file['name'])) {
                    return '';
                }
            }
            if (empty($file['name']) && !empty($data)) {
                return $data['value'];
            }

            $path = $this->_filesystem->getDirectoryRead(
                DirectoryList::MEDIA
            )->getAbsolutePath(
                'blog/image/'
            );
            try {
                /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
                $uploader = $this->_uploaderFactory->create(['fileId' => 'image_content']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                $uploader->setAllowRenameFiles(false);
                $result = $uploader->save($path);
                if (is_array($result) && !empty($result['name'])) {
                    return 'blog/image/'.$result['name'];
                }

            } catch (\Exception $e) {
                if ($e->getCode() != \Magento\MediaStorage\Model\File\Uploader::TMP_NAME_EMPTY) {
                    $this->_logger->critical($e);
                }
            }

            return null;
        }
    }
}