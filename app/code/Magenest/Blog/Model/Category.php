<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\UrlInterface;
use Magento\Framework\Registry;
use Magento\Framework\Model\Context;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class Category
 * @package Magenest\Blog\Model
 *
 * @method ResourceModel\Category _getResource()
 * @method ResourceModel\Category getResource()
 * @method string getIdentifier()
 * @method string getTitle()
 */
class Category extends AbstractModel
{
    /**
     * @param Context $context
     * @param Registry $registry
     * @param UrlInterface $url
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        UrlInterface $url,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_url = $url;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     *
     */
    public function _construct()
    {
        $this->_init('Magenest\Blog\Model\ResourceModel\Category');
    }

    /**
     * Retrieve category url
     * @return string
     */
    public function getCategoryUrl()
    {
        return $this->_url->getUrl('', ['_direct' => 'blog/category/'.$this->getIdentifier()]);
    }

    /**
     * Load By Identifier
     *
     * @param $identifier
     * @return $this
     */
    public function loadByIdentifier($identifier)
    {
        $this->load($identifier, 'identifier');
        return $this;
    }
}
