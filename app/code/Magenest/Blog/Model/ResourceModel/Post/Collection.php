<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Model\ResourceModel\Post;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Collection
 * @package Magenest\Blog\Model\ResourceModel\Post
 */
class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Name prefix of events that are dispatched by model
     *
     * @var string
     */
    protected $_eventPrefix = 'blog_posts';

    /**
     * Name of event parameter
     *
     * @var string
     */
    protected $_eventObject = 'blog_posts';

    /**
     * @param EntityFactory $entityFactory
     * @param LoggerInterface $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface $eventManager
     * @param StoreManagerInterface $storeManager
     * @param null $connection
     * @param AbstractDb|null $resource
     */
    public function __construct(
        EntityFactory $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        StoreManagerInterface $storeManager,
        $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->_storeManager = $storeManager;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Blog\Model\Post', 'Magenest\Blog\Model\ResourceModel\Post');
        $this->_map['fields']['post_id'] = 'main_table.post_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
        $this->_map['fields']['category'] = 'category_table.category_id';
    }

    /**
     * Add category filter to collection
     *
     * @param array|int|\Magenest\Blog\Model\Category  $category
     * @return $this
     */
    public function addCategoryFilter($category)
    {
        if (!$this->getFlag('category_filter_added')) {
            $catId = $category->getId();
            $this->getSelect()->join(
                ['category_table' => $this->getTable('magenest_blog_post_category')],
                $this->getConnection()->quoteInto('category_table.category_id = ?', $catId)
                . ' AND main_table.post_id = store_table.post_id',
                []
            )->group('main_table.post_id');
            $this->setFlag('category_filter_added', true);
        }

        return $this;
    }

    /**
     * Filter By tag
     *
     * @param $tag
     * @return array
     */
    public function addTagToFilter($tag)
    {
        $result = new \Magento\Framework\DataObject();
        $tag = str_replace('+', '', $tag);
        print_r($this->_items);
        foreach ($this->_items as $item) {
            $tags = $item->getData('tag');
            echo $tags;
            $convTags = array_map('trim', explode(',', $tags));
            if (in_array($tag, $convTags)) {
                $result[] = $item;

            }
        }

        return $result;
    }

    /**
     * After Load
     */
    public function _afterLoad()
    {
        foreach ($this->_items as $item) {
            $tag = $item->getData('tag');
            $tags = array_map('trim', explode(',', $tag));
            $item->setTag($tags);
        }
        parent::_afterLoad();
    }

    /**
     * Add store filter to collection
     *
     * @return $this
     */
    public function addStoreFilter()
    {
        if (!$this->getFlag('store_filter_added')) {
            $storeId = $this->_storeManager->getStore()->getId();
            $this->getSelect()->joinRight(
                ['store_table' => $this->getTable('magenest_blog_post_store')],
                $this->getConnection()->quoteInto('(store_table.store_id = ? OR store_table.store_id = 0)', $storeId)
                . ' AND main_table.post_id = store_table.post_id',
                []
            )->group('main_table.post_id');
            $this->setFlag('store_filter_added', true);
        }

        return $this;
    }

    /**
     * Add enable filter to collection
     *
     * @return $this
     */
    public function addActiveFilter()
    {
        return $this->addFieldToFilter('status', 1);
    }
}
