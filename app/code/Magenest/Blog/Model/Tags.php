<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   ThaoPV
 */
namespace Magenest\Blog\Model;

use Magento\Framework\Model\AbstractModel;

class Tags extends AbstractModel
{
    /**
     * Initialize resources
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Magenest\Blog\Model\ResourceModel\Tags');

    }

    /**
     * @param $name
     * @return $this
     */
    public function loadName($name)
    {
        $this->load($name, 'name');
        return $this;
    }
}
