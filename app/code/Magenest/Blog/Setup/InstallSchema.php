<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 *
 * Magenest_Blog extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_Blog
 * @author   <ThaoPV> thaopw@gmail.com
 */
namespace Magenest\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Blog setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if (!$installer->tableExists('magenest_blog_post')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magenest_blog_post')
            )->addColumn(
                'post_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Post ID'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Post Title'
            )->addColumn(
                'meta_keywords',
                Table::TYPE_TEXT,
                '2M',
                ['nullable' => true],
                'Post Meta Keywords'
            )->addColumn(
                'meta_description',
                Table::TYPE_TEXT,
                '2M',
                ['nullable' => true],
                'Post Meta Description'
            )->addColumn(
                'identifier',
                Table::TYPE_TEXT,
                100,
                ['nullable' => true, 'default' => null],
                'Post String Identifier'
            )->addColumn(
                'user',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Post User'
            )->addColumn(
                'image_content',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'image Content'
            )->addColumn(
                'tags',
                Table::TYPE_TEXT,
                255,
                ['nullable' => true],
                'Tags'
            )->addColumn(
                'short_content',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Post Short Content'
            )->addColumn(
                'content',
                Table::TYPE_TEXT,
                '2M',
                [],
                'Post Content'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Post Created At'
            )->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Post Update At'
            )->addColumn(
                'publish_time',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Post Publish Time'
            )->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '1'],
                'Is Post Active'
            )->addIndex(
                $installer->getIdxName('magenest_blog_post', ['identifier']),
                ['identifier']
            )->addIndex(
                $setup->getIdxName(
                    $installer->getTable('magenest_blog_post'),
                    ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
                ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
            )->setComment(
                'Magenest Blog Table'
            );
            $installer->getConnection()->createTable($table);
        }

        /**
         * Create table 'magenest_blog_post_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_blog_post_store')
        )->addColumn(
            'post_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Post ID'
        )->addColumn(
            'store_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('magenest_blog_post_store', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('magenest_blog_post_store', 'post_id', 'magenest_blog_post', 'post_id'),
            'post_id',
            $installer->getTable('magenest_blog_post'),
            'post_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('magenest_blog_post_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Magenest Blog Post To Store'
        );
        $installer->getConnection()->createTable($table);


        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_blog_category')
        )->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Category ID'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Title'
        )->addColumn(
            'meta_keywords',
            Table::TYPE_TEXT,
            '2M',
            ['nullable' => true],
            'Category Meta Keywords'
        )->addColumn(
            'meta_description',
            Table::TYPE_TEXT,
            '2M',
            ['nullable' => true],
            'Category Meta Description'
        )->addColumn(
            'identifier',
            Table::TYPE_TEXT,
            100,
            ['nullable' => true, 'default' => null],
            'Category String Identifier'
        )->addColumn(
            'content_heading',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Content Heading'
        )->addColumn(
            'content',
            Table::TYPE_TEXT,
            '2M',
            [],
            'Category Content'
        )->addColumn(
            'path',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Path'
        )->addColumn(
            'position',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Category Position'
        )->addColumn(
            'status',
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Category Active'
        )->addIndex(
            $installer->getIdxName('magenest_blog_category', ['identifier']),
            ['identifier']
        )->addIndex(
            $setup->getIdxName(
                $installer->getTable('magenest_blog_category'),
                ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )->setComment(
            'Magenest Blog Category Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_blog_category_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_blog_category_store')
        )->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Category ID'
        )->addColumn(
            'store_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )->addIndex(
            $installer->getIdxName('magenest_blog_category_store', ['store_id']),
            ['store_id']
        )->addForeignKey(
            $installer->getFkName('magenest_blog_category_store', 'category_id', 'magenest_blog_category', 'category_id'),
            'category_id',
            $installer->getTable('magenest_blog_category'),
            'category_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('magenest_blog_category_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Magenest Blog Category To Store'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_blog_post_category'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_blog_post_category')
        )->addColumn(
            'post_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Post ID'
        )->addColumn(
            'category_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Category ID'
        )->addIndex(
            $installer->getIdxName('magenest_blog_post_category', ['category_id']),
            ['category_id']
        )->addForeignKey(
            $installer->getFkName('magenest_blog_post_category', 'post_id', 'magenest_blog_post', 'post_id'),
            'post_id',
            $installer->getTable('magenest_blog_post'),
            'post_id',
            Table::ACTION_CASCADE
        )->addForeignKey(
            $installer->getFkName('magenest_blog_post_category', 'category_id', 'magenest_blog_category', 'category_id'),
            'category_id',
            $installer->getTable('magenest_blog_category'),
            'category_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Magenest Blog Post To Category Table'
        );

        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_blog_tags'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_blog_tags')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Tags Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Tag name'
        )->setComment(
            'Magenest Blog Tags table'
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
