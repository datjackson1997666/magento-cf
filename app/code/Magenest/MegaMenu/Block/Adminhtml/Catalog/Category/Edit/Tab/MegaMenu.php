<?php
/**
 * Google Optimizer Category Tab
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\MegaMenu\Block\Adminhtml\Catalog\Category\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Magento\Backend\Block\Widget\Form\Generic;
use Magenest\MegaMenu\Model\CodeFactory;

/**
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class MegaMenu extends Generic
{
    /**
     * @var Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Magenest\MegaMenu\Model\CodeFactory
     */
    protected $_megaMenu;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Config $wysiwygConfig
     * @param CodeFactory $code
     * @param array $data
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Config $wysiwygConfig,
        CodeFactory $code,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_megaMenu = $code;
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        $cateModel = $this->_coreRegistry->registry('current_category');
        $cateId = $cateModel->getId();
        $model = $this->_megaMenu->create()->loadByCategoryId($cateId);
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('megamenu_');

        $fieldset = $form->addFieldset(
            'megamenu_fields',
            ['legend' => __('MegaMenu')]
        );

        if($model->getId()){
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'mega[id]']
            );
        }
        $fieldset->addField(
            'usemegamenu',
            'select',
            [
                'label' => __('Use MegaMenu'),
                'title' => __('Use MegaMenu'),
                'name' => 'mega[usemegamenu]',
                'options' => ['0' => __('No'), '1' => __('Yes')],
            ]
        );

        $fieldset->addField(
            'htmlcode',
            'editor',
            [
                'name' => 'mega[htmlcode]',
                'label' => __('HTML Code'),
                'style' => 'height:36em',
                'required' => false,
                'config' => $this->_wysiwygConfig->getConfig(),
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
