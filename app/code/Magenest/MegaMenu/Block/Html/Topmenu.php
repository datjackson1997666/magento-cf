<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\MegaMenu\Block\Html;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Data\TreeFactory;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magenest\MegaMenu\Model\CodeFactory;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Theme\Block\Html\Topmenu as ThemeTopmenu;
/**
 * Html page top menu block
 */
class Topmenu extends ThemeTopmenu
{
    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var CodeFactory
     */
    protected $_codeFatory;

    /**
     * @param Context $context
     * @param NodeFactory $nodeFactory
     * @param TreeFactory $treeFactory
     * @param CodeFactory $codeFactory
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        NodeFactory $nodeFactory,
        TreeFactory $treeFactory,
        CodeFactory $codeFactory,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        parent::__construct($context, $nodeFactory, $treeFactory, $data);
        $this->_filterProvider = $filterProvider;
        $this->_codeFatory = $codeFactory;
    }

    public function getHtmlCode($short_content)
    {
        return $this->_filterProvider->getPageFilter()->filter($short_content);
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param \Magento\Framework\Data\Tree\Node $menuTree
     * @param string $childrenWrapClass
     * @param int $limit
     * @param array $colBrakes
     * @return string
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getHtml(
        \Magento\Framework\Data\Tree\Node $menuTree,
        $childrenWrapClass,
        $limit,
        $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {
            $arrId = explode('-', $child->getId());
            $id = (int)$arrId[2];
            $megaMenu = $this->_codeFatory->create()->loadByCategoryId($id);

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            $status = $megaMenu->getUsemegamenu();
            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                if ($status) {
                    $outermostClass .= ' megamenu';
                }
                $child->setClass($outermostClass);
            }

            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                    $child->getName()
                ) . '</span></a>';
            if($childLevel == 0 && $status){
                $html .= "<div class='megamenu-content'><div class='megamenu-left'>";
            }

            $html .= $this->_addSubMenu(
                $child,
                $childLevel,
                $childrenWrapClass,
                $limit
            ) ;
            if ($childLevel == 0 && $status) {
                $html .= '</div><div class="megamenu-right">'.$this->getHtmlCode($megaMenu->getHtmlcode()).'</div></div>';
            }

            $html .= '</li>';

            $itemPosition++;
            $counter++;
        }

        if (count($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }
}
