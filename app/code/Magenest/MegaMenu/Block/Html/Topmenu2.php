<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\MegaMenu\Block\Html;

use \Magento\Theme\Block\Html\Topmenu as ThemeTopmenu;

/**
 * Html page top menu block
 */
class Topmenu2 extends ThemeTopmenu
{

}
