<?php
namespace Magenest\MegaMenu\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Code
 * @package Magenest\MegaMenu\Model
 */
class Code extends AbstractModel
{
    /**
     * Init
     */
    protected function _construct()
    {
        $this->_init('Magenest\MegaMenu\Model\ResourceModel\Code');
    }

    /**
     * Load By Category Id
     *
     * @param $id
     * @return $this
     */
    public function loadByCategoryId($id)
    {
        return $this->load($id, 'menu_id');
    }
}
