<?php
namespace Magenest\MegaMenu\Model\ResourceModel\Code;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Magenest\MegaMenu\Model\Code', 'Magenest\MegaMenu\Model\ResourceModel\Code'
        );
    }
}