<?php
/**
 * Google Optimizer Observer Category Tab
 *
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Magenest\MegaMenu\Observer\Block\Category;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\Registry;

class AddTabObserver implements ObserverInterface
{
    protected $_scopeConfig;
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param LayoutInterface $layout
     * @param Registry $registry
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LayoutInterface $layout,
        Registry $registry)
    {
        $this->_registry = $registry;
        $this->_scopeConfig = $scopeConfig;
        $this->_layout = $layout;
    }

    /**
     * Adds Google Experiment tab to the category edit page
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        /** @var $tabs \Magento\Catalog\Block\Adminhtml\Category\Tabs */
        $tabs = $observer->getEvent()->getTabs();
        $model = $this->_registry->registry('current_category');
        $id = $model->getId();
        $level = (int)$model->getLevel();
        if ($id && $level == 2) {
            $content = $this->_layout->createBlock(
                'Magenest\MegaMenu\Block\Adminhtml\Catalog\Category\Edit\Tab\MegaMenu',
                'megamenu'
                )->toHtml();
            $tabs->addTab(
                'megamenu',
                ['label' => __('MegaMenu'), 'content' => $content]
            );
        }
    }
}
