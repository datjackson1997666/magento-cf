<?php
// @codingStandardsIgnoreFile

namespace Magenest\MegaMenu\Observer\Block\Category;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magenest\MegaMenu\Model\CodeFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class SaveCategory
 * @package Magenest\MegaMenu\Observer\Block\Category
 */
class SaveCategory implements ObserverInterface
{
    /**
     * @var CodeFactory
     */
    protected $_megaMenu;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @param RequestInterface $request
     * @param ManagerInterface $messageManager
     * @param CodeFactory $codeFactory
     */
    public function __construct(
        RequestInterface $request,
        ManagerInterface $messageManager,
        CodeFactory $codeFactory
    ) {
        $this->_request = $request;
        $this->_megaMenu = $codeFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * Save Html Code MegaMenu
     *
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $params = $this->_request->getParams();
        if (!empty($params['id']) && !empty($params['mega'])) {
            $cateId = $params['id'];
            $data = $params['mega'];
            $data['menu_id'] = $cateId;
            $model = $this->_megaMenu->create();
            if(!empty($data['id'])){
                $model->load($data['id']);
                unset($data['id']);
            }
            try {
                $model->addData($data);
                $model->save();
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e, __('Something went wrong while saving MegaMenu.'));
            }
        }
    }
}
