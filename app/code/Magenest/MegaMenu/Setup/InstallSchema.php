<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magenest\MegaMenu\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        /**
         * Create table 'magenest_megamenu_html_code'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_megamenu_html_code'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'menu_id',
                Table::TYPE_SMALLINT,
                null,
                [],
                'Menu ID'
            )
            ->addColumn(
                'topmenu',
                Table::TYPE_TEXT,
                null,
                [],
                'Top Menu'
            )
            ->addColumn(
                'htmlcode',
                Table::TYPE_TEXT,
                null,
                [],
                'HTML Code'
            )
            ->addColumn(
                'usemegamenu',
                Table::TYPE_SMALLINT,
                null,
                [],
                'Use MegaMenu'
            );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
