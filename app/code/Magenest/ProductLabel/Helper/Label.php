<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_ProductLabel extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_ProductLabel
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\ProductLabel\Helper;

use Magento\CatalogRule\Model\Rule as RuleModel;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Filesystem;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class ImageBuilder
 *
 * @package Magenest\ProductLabel\Helper\Product
 */
class Label
{
    /**
     * @var RuleModel
     */
    protected $_catalogRule;

    /**
     * @var CollectionFactory
     */
    protected $_productCollection;

    /**
     * Core config data
     *
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var DateTime
     */
    protected $_coreDate;

    /**
     * @param RuleModel $catalogRule
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param CollectionFactory $productCollectionFactory
     * @param DateTime $coreDate
     */
    public function __construct(
        RuleModel $catalogRule,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        CollectionFactory $productCollectionFactory,
        DateTime $coreDate
    ) {
        $this->_catalogRule = $catalogRule;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeManager =$storeManager;
        $this->_coreDate = $coreDate;
        $this->_productCollection = $productCollectionFactory;
        $this->_filesystem = $filesystem;
    }

    /**
     * Set Product
     *
     * @param  Product $product
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->_product = $product;

        return $this;
    }

    /**
     * is New Product
     *
     * @return bool
     */
    public function isNew()
    {
        $days = (int)$this->_scopeConfig->getValue('product_labels/category_new/days');
        $datetime = $this->_coreDate->gmtDate();
        $data = $this->_product->getCreatedAt();
/*
        if ($data) {
            $isNew = (int)strtotime($data) + $days * 86400 - strtotime($datetime);
            if ($isNew > 0) {
                return true;
            }
        }*/

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->_productCollection->create()
            ->addAttributeToFilter('entity_id', $this->_product->getId())
            ->addAttributeToFilter(
                'news_from_date',
                [
                    'or' => [
                        0 => ['date' => true, 'to' => $datetime],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left'
            )->addAttributeToFilter(
                'news_to_date',
                [
                    'or' => [
                        0 => ['date' => true, 'from' => $datetime],
                        1 => ['is' => new \Zend_Db_Expr('null')],
                    ]
                ],
                'left'
            )->addAttributeToFilter(
                [
                    ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                    ['attribute' => 'news_to_date', 'is' => new \Zend_Db_Expr('not null')],
                ]
            )->addAttributeToSort(
                'news_from_date',
                'desc'
            )->getFirstItem();

        if ($product->getId()) {
            return true;
        }

        return false;
    }

    /**
     * Sales Product
     *
     * @return bool
     */
    public function isSale()
    {
        $rule = $this->_catalogRule->calcProductPriceRule($this->_product, $this->_product->getPrice());
        if ($rule) {
            return true;
        }

        $price = $this->_product->getPrice();
        $finalFinal = $this->_product->getFinalPrice();

        if ($this->_product->getTypeId() == 'configurable') {
            $allProducts = $this->_product->getTypeInstance()->getUsedProducts($this->_product, null);
            /** @var \Magento\Catalog\Model\Product $product */
            foreach ($allProducts as $product){
                $subPrice = $product->getPrice();
                $subFinalPrice = $product->getFinalPrice();
                if ($subPrice > $subFinalPrice) {
                    return true;
                }
            }
        }

        if ($price > $finalFinal) {
            return true;
        }

        return false;
    }

    /**
     * @return DataObject
     */
    public function getDefaultCategorySale()
    {
        $display = $this->_scopeConfig->getValue('product_labels/category_onsale/display');
        $_label = new DataObject;
        if (!$display) {
            return $_label;
        }

        $path = "productlabel/config/sale/".$this->_scopeConfig->getValue('product_labels/category_onsale/image');
        $categoryText = $this->_scopeConfig->getValue('product_labels/category_onsale/text');
        $position = $this->_scopeConfig->getValue('product_labels/category_onsale/position');
        $data = [
            'category_image_url' => $this->getLabelImageUrl($path),
            'category_text' => $categoryText,
            'category_position' => $position,
            'category_display' => true,
        ];

        $_label->setData($data);

        return $_label;
    }

    /**
     * Get default label from Configuration
     *
     * @return DataObject
     */
    public function getDefaultProductSale()
    {
        $display = $this->_scopeConfig->getValue('product_labels/product_onsale/display');
        $_label = new DataObject;

        if (!$display) {
            return $_label;
        }

        $path = "productlabel/config/sale/".$this->_scopeConfig->getValue('product_labels/product_onsale/image');
        $categoryText = $this->_scopeConfig->getValue('product_labels/product_onsale/text');
        $position = $this->_scopeConfig->getValue('product_labels/product_onsale/position');
        $data = [
            'product_image_url' => $this->getLabelImageUrl($path),
            'product_text' => $categoryText,
            'product_position' => $position,
            'product_display' => true,
        ];

        $_label->setData($data);

        return $_label;
    }

    /**
     * Get label from Configuration
     *
     * @return DataObject|false
     */
    public function getDefaultCategoryNew()
    {
        $display = $this->_scopeConfig->getValue('product_labels/category_new/display');
        $_label = new DataObject;
        if (!$display) {
            return false;
        }

        $path = "productlabel/config/new/".$this->_scopeConfig->getValue('product_labels/category_new/image');
        $categoryText = $this->_scopeConfig->getValue('product_labels/category_new/text');
        $position = $this->_scopeConfig->getValue('product_labels/category_new/position');
        $data = [
            'category_image_url' => $this->getLabelImageUrl($path),
            'category_text' => $categoryText,
            'category_position' => $position,
            'category_display' => true,
        ];

        $_label->setData($data);

        return $_label;
    }

    /**
     * Get label from Configuration
     *
     * @return DataObject
     */
    public function getDefaultProductNew()
    {
        $display = $this->_scopeConfig->getValue('product_labels/product_new/display');
        $_label = new DataObject;
        if (!$display) {
            return $_label;
        }

        $path = "productlabel/config/new/".$this->_scopeConfig->getValue('product_labels/product_new/image');
        $categoryText = $this->_scopeConfig->getValue('product_labels/product_new/text');
        $position = $this->_scopeConfig->getValue('product_labels/product_new/position');

        $data = [
            'product_image_url' => $this->getLabelImageUrl($path),
            'product_text' => $categoryText,
            'product_position' => $position,
            'product_display' => true,
        ];

        $_label->setData($data);

        return $_label;
    }

    /**
     * @param  string $path
     * @return string
     */
    public function getLabelImageUrl($path)
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $url = $mediaUrl.$path;
        return $url;
    }
}
