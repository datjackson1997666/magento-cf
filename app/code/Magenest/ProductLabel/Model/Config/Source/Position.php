<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 *
 * Magenest_ProductLabel extension
 * NOTICE OF LICENSE
 *
 * @category Magenest
 * @package  Magenest_ProductLabel
 * @author   ThaoPV <thaopw@gmail.com>
 */
namespace Magenest\ProductLabel\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Position
 *
 * @package Magenest\ProductLabel\Model\Config\Source
 */
class Position implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'top-left', 'label' => __('Top-Left')],
            ['value' => 'top-right', 'label' => __('Top-Right')],
            ['value' => 'bot-left', 'label' => __('Bottom-Left')],
            ['value' => 'bot-right', 'label' => __('Bottom-Right')]


        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'top-left' => __('Top-Left'),
            'top-right' => __('Top-Right'),
            'bottom-left' => __('Bottom-Left'),
            'bottom-right' => __('Bottom-Right')
        ];
    }
}
