<?php
namespace Magenest\Widget\Block\Product;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Catalog\Pricing\Price;
use Magento\Framework\Pricing\Render;
use Magento\Catalog\Block\Product\Widget\NewWidget;


/**
 * Class HotSalesProduct
 * @package Magenest\Widget\Block\Product
 *
 * @method HotSalesProduct setProductCollection(\Magento\Catalog\Model\ResourceModel\Product\Collection $collection)
 */
class HotSalesProduct extends NewWidget implements IdentityInterface
{
    /**
     * Prepare and return product collection
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection|Object|\Magento\Framework\Data\Collection
     */
    protected function _getProductCollection()
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create();
        $collection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices(
            $collection
        )->addStoreFilter()
            ->setPageSize(
                $this->getProductsCount()
            )->setCurPage(
                1
            );
        return $collection;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        if (!$this->hasData('title')) {
            $this->setData('title', __('Hot Sales Products'));
        }

        return $this->getData('title');
    }

}
