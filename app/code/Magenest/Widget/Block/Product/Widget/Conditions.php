<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Product Chooser for "Product Link" Cms Widget Plugin
 */
namespace Magenest\Widget\Block\Product\Widget;

use Magento\CatalogWidget\Block\Product\Widget\Conditions as ConditionsWidget;

/**
 * Class Conditions
 */
class Conditions extends ConditionsWidget
{
    /**
     * @var string
     */
    protected $_template = 'product/widget/conditions.phtml';

    /**
     * @return string
     */
    public function getCategoryUrl()
    {
        $params = [
            'attribute' => 'category_ids',
            'form' => 'options_fieldset'.md5('Magenest\Widget\Block\Product\ProductsList'),
        ];

        return $this->getUrl('catalog_rule/promo_widget/chooser', $params);
    }
}
