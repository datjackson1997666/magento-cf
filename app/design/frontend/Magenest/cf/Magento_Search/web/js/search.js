require([
    'jquery'
], function($){
    $('.search-clear').on('click',function(){
        $('input#search').val('');
    });
    $('.fa.fa-times').on('click',function(){
        $('.nav-sections').toggleClass('active');
        $('.block-search').toggleClass('active');
    })
    $('label.label').on('click',function(){
        $('.nav-sections').toggleClass('active');
        $('.block-search').toggleClass('active');
    })
})