/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'js/owl.carousel',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';
    console.log('in master');
    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 && $('#co-shipping-method-form .fieldset.rates :checked').length === 0) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header .header.links').clone().appendTo('#store\\.links');

    $('[data-action="toggle-menu"]').click(function() {

        if ($('html').hasClass('nav-open')) {
            $('html').removeClass('nav-open');
            setTimeout(function () {
                $('html').removeClass('nav-before-open');
            }, 300);
        } else {
            $('html').addClass('nav-before-open');
            setTimeout(function () {
                $('html').addClass('nav-open');
            }, 42);
        }
    }) ;
    
    $(".block-blog .post-list").owlCarousel({
        
        items : 3,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,3],
        itemsTabletSmall: [768,2],
        itemsMobile : [479,1],
    });
    $(".owl-slide-style1").owlCarousel({
        navigationText : ["",""],
        autoPlay : 4000,
        stopOnHover : true,
        navigation:true,
        singleItem : true,
        autoHeight : true,
    });
    $(".owl-slide-style2, .owl-slide-style3").owlCarousel({
        autoPlay : 4000,
        stopOnHover : true,
        singleItem : true,
        autoHeight : true,
        pagination:true,
    });
    $(".block-new-style1 .block-new-products .product-items").owlCarousel({
        navigation:true,
        navigationText : ["",""],
        autoPlay : 5000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,4],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });
    $(".block-sales-style1 .product-items").owlCarousel({
        navigation:true,
        navigationText : ["",""],
        autoPlay : 5000,
        stopOnHover : true,
        items : 2,
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,2],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });
    /*

    */
    $(".block-sales-style2 .product-items").owlCarousel({
        navigation:true,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        autoPlay : 5000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,4],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });
    /*$(".megamenu-right .product-items").owlCarousel({
        navigation:true,
        navigationText : ["",""],
        autoPlay : 5000,
        stopOnHover : true,
        items : 2,
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [980,2],
        itemsTablet: [768,2],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });*/
    $(".block-tab-products .product-items").owlCarousel({
        navigation:true,
        navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
        autoPlay : 5000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,4],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });

    $(".block.upsell .product-items, .block.related .product-items").owlCarousel({
        navigation:true,
        navigationText : ["",""],
        autoPlay : 5000,
        items : 4,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,4],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });
    $(".block.crosssell .product-items").owlCarousel({
        navigation:true,
        navigationText : ["",""],
        autoPlay : 5000,
        items : 5,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [980,5],
        itemsTablet: [768,3],
        itemsTabletSmall: false,
        itemsMobile : [479,2],
    });
    $(".owl-brand-style2 ").owlCarousel({
        autoPlay : 5000,
        stopOnHover : true,
        items : 6,
        itemsCustom : false,
        itemsDesktop : [1199,6],
        itemsDesktopSmall : [980,5],
        itemsTablet: [768,4],
        itemsTabletSmall: false,
        itemsMobile : [479,3],       
    });
    

    $( ".megamenu" ).hover(
        function() {
            $( this ).children(".megamenu-content").css( "display" ,"block"); 
        }, function() {
            $( this ).children(".megamenu-content").css( "display" ,"none"); ;
        }
    );

   
   
        
        $('.btn-number').click(function(e){
            e.preventDefault();
            
            var fieldName = $(this).attr('data-field');
            var type      = $(this).attr('data-type');
            var input = $("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    
                    if(currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    } 
                    if(parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('maxlength')) {
                        input.val(currentVal + 1).change();
                    }
                    if(parseInt(input.val()) == input.attr('maxlength')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function(){
           $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {
            
            var minValue =  parseInt($(this).attr('min'));
            var maxValue =  parseInt($(this).attr('maxlength'));
            var valueCurrent = parseInt($(this).val());
            
            name = $(this).attr('name');
            if(valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            
            
        });
        $(".input-number").keydown(function (e) {
               
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                    
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                    
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                        
                         return;
                }
               
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
        });
        
        $(".block-teams").owlCarousel({
            navigation : false,
            autoPlay : 5000,
            items : 6,
            itemsDesktop : [1199,6],
            itemsDesktopSmall : [980,6],
            itemsTablet: [768,4],
            itemsTabletSmall: false,
            itemsMobile : [479,3],
        });
        $(".owl-client-style1").owlCarousel({
            autoPlay : 4000,
            stopOnHover : true,
            singleItem : true,
            pagination:true,
        });
        $(".owl-brand-style1").owlCarousel({
            autoPlay : 5000,
            stopOnHover : true,
            items : 3,
            itemsDesktop : [1199,3],
            itemsDesktopSmall : [980,3],
            itemsTablet: [768,3],
            itemsTabletSmall: false,
            itemsMobile : [479,3],
        });
        $(".footer-columns .widget ").click(function(){
            $(this).children(".block-content").slideDown();
        });

    keyboardHandler.apply();
});
